package de.codecamp.properties;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

import de.codecamp.properties.demo.GenericBeanReference;
import de.codecamp.properties.demo.GenericBeanSubtype;
import de.codecamp.properties.demo.MarkedBean;
import de.codecamp.properties.demo.PGenericBeanReference;
import de.codecamp.properties.demo.PGenericBeanSubtype;
import de.codecamp.properties.demo.PTestPropertyTypes;
import de.codecamp.properties.demo.TestPropertyTypes;


public class PropertiesTest
{

  @Test
  void generics()
  {
    GenericBeanSubtype bean = new GenericBeanSubtype();
    PGenericBeanSubtype.content.set(bean, "test");
    System.out.println(bean.getContent());

    GenericBeanReference ref = new GenericBeanReference();
    PGenericBeanReference.genericBean().content().set(ref, "test2");
  }

  @Test
  void getBooleanProperty()
  {
    TestPropertyTypes bean = new TestPropertyTypes();
    bean.setBooleanProperty(true);
    assumeTrue(bean.isBooleanProperty());

    assertThat(PTestPropertyTypes.booleanProperty.get(bean)).isTrue();
    assertThat(PTestPropertyTypes.booleanProperty().get(bean)).isTrue();
  }

  @Test
  void setBooleanProperty()
  {
    TestPropertyTypes bean = new TestPropertyTypes();
    assumeTrue(!bean.isBooleanProperty());

    PTestPropertyTypes.booleanProperty.set(bean, true);
    assertThat(bean.isBooleanProperty()).isTrue();
    PTestPropertyTypes.booleanProperty().set(bean, false);
    assertThat(bean.isBooleanProperty()).isFalse();
  }

  @Test
  void getBooleanObjectProperty()
  {
    TestPropertyTypes bean = new TestPropertyTypes();

    bean.setBooleanObjectProperty(null);
    assumeTrue(bean.getBooleanObjectProperty() == null);
    assertThat(PTestPropertyTypes.booleanObjectProperty.get(bean)).isNull();
    assertThat(PTestPropertyTypes.booleanObjectProperty().get(bean)).isNull();

    bean.setBooleanObjectProperty(true);
    assumeTrue(bean.getBooleanObjectProperty());
    assertThat(PTestPropertyTypes.booleanObjectProperty.get(bean)).isTrue();
    assertThat(PTestPropertyTypes.booleanObjectProperty().get(bean)).isTrue();
  }

  @Test
  void getGenericsProperty()
  {
    List<String> list = Arrays.asList("test1");

    TestPropertyTypes bean = new TestPropertyTypes();
    bean.setGenericsProperty(list);
    assumeTrue(list.equals(bean.getGenericsProperty()));

    assertThat(PTestPropertyTypes.genericsProperty.get(bean)).asList()
        .containsExactlyElementsOf(list);
    assertThat(PTestPropertyTypes.genericsProperty().get(bean)).asList()
        .containsExactlyElementsOf(list);
  }

  @Test
  void setGenericsProperty()
  {
    TestPropertyTypes bean = new TestPropertyTypes();
    assumeTrue(bean.getGenericsProperty() == null);

    List<String> list = Arrays.asList("test1");

    PTestPropertyTypes.genericsProperty.set(bean, list);
    assertThat(bean.getGenericsProperty()).asList().containsExactlyElementsOf(list);
    PTestPropertyTypes.genericsProperty().set(bean, null);
    assertThat(bean.getGenericsProperty()).isNull();
  }

  @Test
  void nestedProperty()
  {
    MarkedBean nestedBean = new MarkedBean();
    nestedBean.setMarkedBeanProperty("string");
    TestPropertyTypes bean = new TestPropertyTypes();

    assumeTrue(bean.getNestedBeanProperty() == null);

    assertThat(PTestPropertyTypes.nestedBeanProperty().markedBeanProperty().get(bean)).isNull();
    assertThatExceptionOfType(IllegalArgumentException.class).isThrownBy(() -> {
      PTestPropertyTypes.nestedBeanProperty().markedBeanProperty().set(bean, "newString");
    });


    bean.setNestedBeanProperty(nestedBean);
    assumeTrue(bean.getNestedBeanProperty() != null);

    assertThat(PTestPropertyTypes.nestedBeanProperty().markedBeanProperty().get(bean))
        .isEqualTo("string");
    PTestPropertyTypes.nestedBeanProperty().markedBeanProperty().set(bean, "newString");
    assertThat(PTestPropertyTypes.nestedBeanProperty().markedBeanProperty().get(bean))
        .isEqualTo("newString");
  }

}
