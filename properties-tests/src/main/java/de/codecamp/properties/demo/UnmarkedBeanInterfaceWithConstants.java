package de.codecamp.properties.demo;

import de.codecamp.properties.PropertiesFor;


public interface UnmarkedBeanInterfaceWithConstants
{

  String getUnmarkedBeanInterfaceProperty();

  void setUnmarkedBeanInterfaceProperty(String unmarkedBeanInterfaceProperty);



  @PropertiesFor(UnmarkedBeanInterfaceWithConstants.class)
  interface Bla
  {
  }

}
