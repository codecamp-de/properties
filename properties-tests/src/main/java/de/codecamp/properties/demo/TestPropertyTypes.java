package de.codecamp.properties.demo;

import java.util.List;
import java.util.concurrent.TimeUnit;

import de.codecamp.properties.Properties;


@Properties
public class TestPropertyTypes
{

  private boolean booleanProperty;

  private Boolean booleanObjectProperty;

  private long longProperty;

  private String stringProperty;

  private TimeUnit enumProperty;

  private List<String> genericsProperty;

  private MarkedBean nestedBeanProperty;

  private UnmarkedBeanWithConstants unmarkedBeanWithConstantsProperty;

  private UnmarkedBeanNoConstants unmarkedBeanNoConstantsProperty;


  public boolean isBooleanProperty()
  {
    return booleanProperty;
  }

  public void setBooleanProperty(boolean booleanProperty)
  {
    this.booleanProperty = booleanProperty;
  }

  public Boolean getBooleanObjectProperty()
  {
    return booleanObjectProperty;
  }

  public void setBooleanObjectProperty(Boolean booleanObjectProperty)
  {
    this.booleanObjectProperty = booleanObjectProperty;
  }

  public long getLongProperty()
  {
    return longProperty;
  }

  public void setLongProperty(long longProperty)
  {
    this.longProperty = longProperty;
  }

  public String getStringProperty()
  {
    return stringProperty;
  }

  public void setStringProperty(String stringProperty)
  {
    this.stringProperty = stringProperty;
  }

  public TimeUnit getEnumProperty()
  {
    return enumProperty;
  }

  public void setEnumProperty(TimeUnit enumProperty)
  {
    this.enumProperty = enumProperty;
  }

  public List<String> getGenericsProperty()
  {
    return genericsProperty;
  }

  public void setGenericsProperty(List<String> genericsProperty)
  {
    this.genericsProperty = genericsProperty;
  }

  public MarkedBean getNestedBeanProperty()
  {
    return nestedBeanProperty;
  }

  public void setNestedBeanProperty(MarkedBean markedBeanProperty)
  {
    this.nestedBeanProperty = markedBeanProperty;
  }

  public UnmarkedBeanWithConstants getUnmarkedBeanWithConstantsProperty()
  {
    return unmarkedBeanWithConstantsProperty;
  }

  public void setUnmarkedBeanWithConstantsProperty(UnmarkedBeanWithConstants unmarkedBeanProperty)
  {
    this.unmarkedBeanWithConstantsProperty = unmarkedBeanProperty;
  }

  public UnmarkedBeanNoConstants getUnmarkedBeanNoConstantsProperty()
  {
    return unmarkedBeanNoConstantsProperty;
  }

  public void setUnmarkedBeanNoConstantsProperty(
      UnmarkedBeanNoConstants unmarkedBeanNoConstantsProperty)
  {
    this.unmarkedBeanNoConstantsProperty = unmarkedBeanNoConstantsProperty;
  }

}
