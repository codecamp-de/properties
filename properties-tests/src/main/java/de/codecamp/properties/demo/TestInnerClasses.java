package de.codecamp.properties.demo;

import de.codecamp.properties.Properties;


public class TestInnerClasses
{

  /**
   * Property constants for inner classes are created in a top-level type. Its name consists of the
   * inner class and all outer class names, separated by a "_".
   */
  @Properties
  public static class InnerClass
  {

    private String innerClassProperty;


    public String getInnerClassProperty()
    {
      return innerClassProperty;
    }

    public void setInnerClassProperty(String innerClassProperty)
    {
      this.innerClassProperty = innerClassProperty;
    }

  }

}
