package de.codecamp.properties.demo;

import de.codecamp.properties.Properties;


@Properties
public interface MarkedBeanInterface
{

  String getBeanInterfaceProperty();

  void setBeanInterfaceProperty(String beanInterfaceProperty);

}
