package de.codecamp.properties.demo;

import de.codecamp.properties.PropertiesFor;


public class UnmarkedBeanWithConstants
{

  private String unmarkedBeanProperty;


  public String getUnmarkedBeanProperty()
  {
    return unmarkedBeanProperty;
  }

  public void setUnmarkedBeanProperty(String unmarkedBeanProperty)
  {
    this.unmarkedBeanProperty = unmarkedBeanProperty;
  }



  @PropertiesFor(UnmarkedBeanWithConstants.class)
  interface Bla
  {
  }

}
