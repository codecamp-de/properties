package de.codecamp.properties.demo;

import de.codecamp.properties.Properties;


@Properties
public class MarkedBean
{

  private String markedBeanProperty;


  // property name that is a keyword
  public boolean isNew()
  {
    return true;
  }


  public String getMarkedBeanProperty()
  {
    return markedBeanProperty;
  }

  public void setMarkedBeanProperty(String stringProperty)
  {
    this.markedBeanProperty = stringProperty;
  }

}
