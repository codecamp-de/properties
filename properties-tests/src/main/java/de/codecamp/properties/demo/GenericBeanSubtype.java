package de.codecamp.properties.demo;

import java.lang.reflect.Method;

import de.codecamp.properties.Properties;


@Properties
public class GenericBeanSubtype
  extends GenericBean<String>
{

  public static void main(String[] args)
  {
    for (Method method : GenericBeanSubtype.class.getMethods())
    {
      System.out.println(method);
      System.out.println(method.getGenericReturnType());
    }
  }


}
