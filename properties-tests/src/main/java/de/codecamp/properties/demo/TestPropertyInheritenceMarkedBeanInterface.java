package de.codecamp.properties.demo;

import de.codecamp.properties.Properties;


@Properties
public class TestPropertyInheritenceMarkedBeanInterface
  implements MarkedBeanInterface
{

  private String uninheritedProperty;


  public String getUninheritedProperty()
  {
    return uninheritedProperty;
  }

  public void setUninheritedProperty(String uninheritedProperty)
  {
    this.uninheritedProperty = uninheritedProperty;
  }

  @Override
  public String getBeanInterfaceProperty()
  {
    return null;
  }

  @Override
  public void setBeanInterfaceProperty(String beanInterfaceProperty)
  {
  }

}
