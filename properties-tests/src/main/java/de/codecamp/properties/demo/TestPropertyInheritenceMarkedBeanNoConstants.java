package de.codecamp.properties.demo;

import de.codecamp.properties.Properties;


@Properties
public class TestPropertyInheritenceMarkedBeanNoConstants
  extends UnmarkedBeanNoConstants
{

  private String uninheritedProperty;


  public String getUninheritedProperty()
  {
    return uninheritedProperty;
  }

  public void setUninheritedProperty(String uninheritedProperty)
  {
    this.uninheritedProperty = uninheritedProperty;
  }

}
