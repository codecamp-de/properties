package de.codecamp.properties.demo;

import de.codecamp.properties.Properties;


@Properties
public class TestPropertyInheritenceMarkedBean
  extends MarkedBean
{

  private String uninheritedProperty;


  public String getUninheritedProperty()
  {
    return uninheritedProperty;
  }

  public void setUninheritedProperty(String uninheritedProperty)
  {
    this.uninheritedProperty = uninheritedProperty;
  }

}
