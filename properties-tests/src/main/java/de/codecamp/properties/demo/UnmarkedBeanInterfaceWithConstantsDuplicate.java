package de.codecamp.properties.demo;

import de.codecamp.properties.PropertiesFor;


public interface UnmarkedBeanInterfaceWithConstantsDuplicate
{

  String getUnmarkedBeanInterfaceProperty();

  void setUnmarkedBeanInterfaceProperty(String unmarkedBeanInterfaceProperty);



  @PropertiesFor(UnmarkedBeanInterfaceWithConstantsDuplicate.class)
  interface Bla
  {
  }

}
