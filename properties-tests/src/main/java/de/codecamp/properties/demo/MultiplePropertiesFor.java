package de.codecamp.properties.demo;

import de.codecamp.properties.PropertiesFor;


@PropertiesFor({MultiplePropertiesFor.Bean1.class, MultiplePropertiesFor.Bean2.class})
public class MultiplePropertiesFor
{

  public static class Bean1
  {
    public String getProperty()
    {
      return null;
    }
  }

  public static class Bean2
  {
    public String getProperty()
    {
      return null;
    }
  }

}
