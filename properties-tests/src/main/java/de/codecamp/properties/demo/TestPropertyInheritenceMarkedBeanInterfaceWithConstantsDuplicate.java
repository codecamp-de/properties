package de.codecamp.properties.demo;

import de.codecamp.properties.Properties;


@Properties
public class TestPropertyInheritenceMarkedBeanInterfaceWithConstantsDuplicate
  implements UnmarkedBeanInterfaceWithConstants, UnmarkedBeanInterfaceWithConstantsDuplicate
{

  private String uninheritedProperty;


  void test()
  {
    /*
     * XXX The same property from different sources is ambiguous here; it's no problem for the bean
     * but using the property constant will fail. Nothing that can be done about it.
     */

    // System.out.println(
    // TestPropertyInheritenceMarkedBeanInterfaceWithConstantsDuplicate__.unmarkedBeanInterfaceProperty);
  }

  public String getUninheritedProperty()
  {
    return uninheritedProperty;
  }

  public void setUninheritedProperty(String uninheritedProperty)
  {
    this.uninheritedProperty = uninheritedProperty;
  }

  @Override
  public String getUnmarkedBeanInterfaceProperty()
  {
    return null;
  }

  @Override
  public void setUnmarkedBeanInterfaceProperty(String unmarkedBeanInterfaceProperty)
  {
  }

}
