package de.codecamp.properties.demo;

public interface UnmarkedBeanInterfaceNoConstantsDuplicate
{

  String getUnmarkedBeanInterfaceProperty();

  void setUnmarkedBeanInterfaceProperty(String unmarkedBeanInterfaceProperty);

}
