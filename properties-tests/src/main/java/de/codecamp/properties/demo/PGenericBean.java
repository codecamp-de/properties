// package de.codecamp.properties.demo;
//
// import javax.annotation.Generated;
//
// import de.codecamp.properties.Property;
// import de.codecamp.properties.PropertyPath;
// import de.codecamp.properties.impl.PropertyImpl;
// import de.codecamp.properties.impl.PropertyPathImpl;
//
//
// @Generated("de.codecamp.properties.processor.PropertiesProcessor")
// public interface PGenericBean
// {
//
// // TODO don't provide setter because generics wouldn't usually allow setting Object?
// Property<GenericBean<Object>, Object> content =
// new PropertyImpl<>("content", GenericBean::getContent, GenericBean::setContent);
//
//
// static PropertyPath<GenericBean<Object>, Object> content()
// {
// return new PropertyPathImpl<>(null, content);
// }
//
//
// class Path_<S, T extends GenericBean<Object>>
// extends PropertyPathImpl<S, T>
// implements Props<S, Object>
// {
// public <M> Path_(PropertyPath<S, M> path, Property<M, T extends GenericBean<Object>> property)
// {
// super(path, property);
// }
// }
//
//
// interface Props<S, T>
// extends PropertyPath<S>
// {
// default PropertyPath<S, T> content()
// {
// return (PropertyPath<S, T>) new PropertyPathImpl<S, Object>(this, PGenericBean.content);
// }
// }
//
// }
