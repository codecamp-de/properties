package de.codecamp.properties.demo;

import de.codecamp.properties.Properties;


@Properties
public class TestPropertyInheritenceMarkedBeanInterfaceWithConstants
  implements UnmarkedBeanInterfaceWithConstants
{

  private String uninheritedProperty;


  public String getUninheritedProperty()
  {
    return uninheritedProperty;
  }

  public void setUninheritedProperty(String uninheritedProperty)
  {
    this.uninheritedProperty = uninheritedProperty;
  }

  @Override
  public String getUnmarkedBeanInterfaceProperty()
  {
    return null;
  }

  @Override
  public void setUnmarkedBeanInterfaceProperty(String unmarkedBeanInterfaceProperty)
  {
  }

}
