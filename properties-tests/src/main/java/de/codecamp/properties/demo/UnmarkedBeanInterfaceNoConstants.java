package de.codecamp.properties.demo;

public interface UnmarkedBeanInterfaceNoConstants
{

  String getUnmarkedBeanInterfaceProperty();

  void setUnmarkedBeanInterfaceProperty(String unmarkedBeanInterfaceProperty);

}
