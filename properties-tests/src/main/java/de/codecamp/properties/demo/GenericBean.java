package de.codecamp.properties.demo;

import de.codecamp.properties.Properties;


@Properties
public class GenericBean<T>
{

  private T content;


  public T getContent()
  {
    return content;
  }

  public void setContent(T content)
  {
    this.content = content;
  }

}
