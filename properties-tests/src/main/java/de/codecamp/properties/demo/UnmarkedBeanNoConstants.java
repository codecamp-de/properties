package de.codecamp.properties.demo;


public class UnmarkedBeanNoConstants
{

  private String unmarkedBeanProperty;


  public String getUnmarkedBeanProperty()
  {
    return unmarkedBeanProperty;
  }

  public void setUnmarkedBeanProperty(String unmarkedBeanProperty)
  {
    this.unmarkedBeanProperty = unmarkedBeanProperty;
  }

}
