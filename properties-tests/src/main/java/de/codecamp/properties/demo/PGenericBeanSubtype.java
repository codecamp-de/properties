// package de.codecamp.properties.demo;
//
// import javax.annotation.Generated;
//
// import de.codecamp.properties.Property;
// import de.codecamp.properties.PropertyPath;
// import de.codecamp.properties.impl.PropertyImpl;
// import de.codecamp.properties.impl.PropertyPathImpl;
//
//
// @Generated("de.codecamp.properties.processor.PropertiesProcessor")
// public interface PGenericBeanSubtype
// extends PGenericBean
// {
//
// Property<GenericBeanSubtype, String> content =
// new PropertyImpl<>("content", GenericBeanSubtype::getContent, GenericBeanSubtype::setContent);
//
//
// static PropertyPath<GenericBeanSubtype, String> content()
// {
// return new PropertyPathImpl<>(null, content);
// }
//
//
// class Path_<S>
// extends PGenericBean.Path_<S>
// {
// public <M> Path_(PropertyPath<S, M> path, Property<M, GenericBeanSubtype> property)
// {
// super(path, property);
// }
//
// @Override
// public PropertyPath<S, String> content()
// {
// return new PropertyPathImpl<>(this, PGenericBeanSubtype.content);
// }
// }
//
// interface Props<S>
// extends PGenericBean.Props<S, String>
// {
// @Override
// default PropertyPath<S, String> content()
// {
// return new PropertyPathImpl<>(this, PGenericBeanSubtype.content);
// }
// }
//
// }
