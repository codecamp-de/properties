package de.codecamp.properties.demo;

import de.codecamp.properties.Properties;


@Properties
public class GenericBeanReference
{

  private GenericBeanSubtype genericBean = new GenericBeanSubtype();


  public GenericBeanSubtype getGenericBean()
  {
    return genericBean;
  }

  public void setGenericBean(GenericBeanSubtype genericBean)
  {
    this.genericBean = genericBean;
  }

}
