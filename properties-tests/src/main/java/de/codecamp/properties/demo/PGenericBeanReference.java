// package de.codecamp.properties.demo;
//
// import javax.annotation.Generated;
//
// import de.codecamp.properties.Property;
// import de.codecamp.properties.PropertyPath;
// import de.codecamp.properties.impl.PropertyImpl;
// import de.codecamp.properties.impl.PropertyPathImpl;
//
//
// @Generated("de.codecamp.properties.processor.PropertiesProcessor")
// public interface PGenericBeanReference
// {
//
// Property<GenericBeanReference, GenericBeanSubtype> genericBean = new
// PropertyImpl<>("genericBean",
// GenericBeanReference::getGenericBean, GenericBeanReference::setGenericBean);
//
//
// static PropertyPath<GenericBeanReference, GenericBeanSubtype> genericBean()
// {
// return new PGenericBeanSubtype.Path_<>(null, genericBean);
// }
//
//
// class Path_<S>
// extends PropertyPathImpl<S, GenericBean<Object>>
// implements Props<S, Object>
// {
// public <M> Path_(PropertyPath<S, M> path, Property<M, GenericBean<Object>> property)
// {
// super(path, property);
// }
// }
//
// interface Props<S, T>
// {
// default PropertyPath<S, T> genericBean()
// {
// return new PropertyPathImpl<S, T>(this, PGenericBeanReference.genericBean);
// }
// }
//
// }
