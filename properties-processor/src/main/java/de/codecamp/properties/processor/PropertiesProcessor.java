package de.codecamp.properties.processor;

import static java.util.stream.Collectors.joining;
import static javax.lang.model.util.ElementFilter.methodsIn;

import java.beans.Introspector;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.processing.AbstractProcessor;
import javax.annotation.processing.ProcessingEnvironment;
import javax.annotation.processing.Processor;
import javax.annotation.processing.RoundEnvironment;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.Modifier;
import javax.lang.model.element.PackageElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.DeclaredType;
import javax.lang.model.type.ExecutableType;
import javax.lang.model.type.TypeKind;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.type.TypeVariable;
import javax.tools.Diagnostic.Kind;

import com.google.auto.service.AutoService;
import com.squareup.javapoet.AnnotationSpec;
import com.squareup.javapoet.ClassName;
import com.squareup.javapoet.FieldSpec;
import com.squareup.javapoet.JavaFile;
import com.squareup.javapoet.MethodSpec;
import com.squareup.javapoet.ParameterizedTypeName;
import com.squareup.javapoet.TypeName;
import com.squareup.javapoet.TypeSpec;
import com.squareup.javapoet.TypeVariableName;

import de.codecamp.properties.Properties;
import de.codecamp.properties.PropertiesFor;
import de.codecamp.properties.Property;
import de.codecamp.properties.PropertyPath;
import de.codecamp.properties.impl.PropertyImpl;
import de.codecamp.properties.impl.PropertyPathImpl;


/**
 * Creates name and {@link Property} constants for the bean property of {@link Properties} annotated
 * classes, and classes referenced in {@PropertiesFor}.
 */
@AutoService(Processor.class)
public class PropertiesProcessor
  extends AbstractProcessor
{

  private static final Logger LOG = Logger.getLogger(PropertiesProcessor.class.getName());


  private static final Set<String> SUPPORTED_ANNOTATIONS = Collections.unmodifiableSet(
      new HashSet<>(Arrays.asList(Properties.class.getName(), PropertiesFor.class.getName())));


  static final String PROPERTIES_PREFIX = "P";

  static final String PROPERTIES_SUFFIX = "";

  static final String STRING_CONSTANTS_SUFFIX = "_";

  static final String INNER_PROPERTY = "Property_";

  static final String INNER_PATH = "Path_";


  static final String SET_PREFIX = "set";

  static final String GET_PREFIX = "get";

  static final String IS_PREFIX = "is";

  static final Set<String> IGNORED_PROPERTIES = new HashSet<>(Arrays.asList("class"));


  static final TypeVariableName TYPEVAR_S = TypeVariableName.get("S");

  static final TypeVariableName TYPEVAR_M = TypeVariableName.get("M");

  static final TypeVariableName TYPEVAR_T = TypeVariableName.get("T");


  private ProcessorUtils utils;

  private Set<String> processedBeanTypes = new HashSet<>();



  @Override
  public SourceVersion getSupportedSourceVersion()
  {
    return SourceVersion.latest();
  }

  @Override
  public Set<String> getSupportedAnnotationTypes()
  {
    return SUPPORTED_ANNOTATIONS;
  }


  @Override
  public synchronized void init(ProcessingEnvironment processingEnv)
  {
    super.init(processingEnv);

    utils = new ProcessorUtils(processingEnv);
  }


  @Override
  public boolean process(Set<? extends TypeElement> annotations, RoundEnvironment roundEnv)
  {
    for (Element element : roundEnv.getElementsAnnotatedWith(PropertiesFor.class))
    {
      AnnotationMirror am = utils.getAnnotationMirror(element, PropertiesFor.class);
      for (TypeElement beanTypeElement : utils.getAnnotationValueAsTypes(am, "value"))
      {
        processBeanType(beanTypeElement);
      }
    }

    for (Element element : roundEnv.getElementsAnnotatedWith(Properties.class))
    {
      processBeanType((TypeElement) element);
    }

    if (roundEnv.processingOver())
    {
      processedBeanTypes.clear();
    }

    return false;
  }

  protected void processBeanType(TypeElement beanTypeElement)
  {
    // already processed?
    if (processedBeanTypes.contains(beanTypeElement.getQualifiedName().toString()))
      return;


    AnnotationSpec generatedAtSpec = utils.getGeneratedAnnotation(processingEnv.getElementUtils())
        .map(ClassName::get).map(genClassName -> AnnotationSpec.builder(genClassName)
            .addMember("value", "$S", getClass().getName()).build())
        .orElse(null);

    PackageElement packageElement = processingEnv.getElementUtils().getPackageOf(beanTypeElement);

    try
    {
      ClassName beanPropertiesTypeName = getPropertyPathConstantsClassName(beanTypeElement);
      TypeSpec.Builder propertiesClassBuilder =
          TypeSpec.interfaceBuilder(beanPropertiesTypeName).addModifiers(Modifier.PUBLIC);
      if (generatedAtSpec != null)
        propertiesClassBuilder.addAnnotation(generatedAtSpec);

      TypeSpec.Builder pathClassBuilder =
          TypeSpec.classBuilder(INNER_PATH).addTypeVariable(TYPEVAR_S) //
              .addModifiers(Modifier.PUBLIC, Modifier.STATIC) //
              .superclass(ParameterizedTypeName.get(ClassName.get(PropertyPathImpl.class),
                  TYPEVAR_S, ClassName.get(beanTypeElement)))
              .addMethod(MethodSpec.constructorBuilder().addModifiers(Modifier.PUBLIC)
                  .addTypeVariable(TYPEVAR_M)
                  .addParameter(ParameterizedTypeName.get(ClassName.get(PropertyPath.class),
                      TYPEVAR_S, TYPEVAR_M), "path")
                  .addParameter(ParameterizedTypeName.get(ClassName.get(Property.class), TYPEVAR_M,
                      ClassName.get(beanTypeElement)), "property")
                  .addStatement("super(path, property)").build());


      ClassName stringConstantsTypeName = getStringConstantsClassName(beanTypeElement);
      TypeSpec.Builder stringConstantsClassBuilder =
          TypeSpec.interfaceBuilder(stringConstantsTypeName).addModifiers(Modifier.PUBLIC);
      if (generatedAtSpec != null)
        stringConstantsClassBuilder.addAnnotation(generatedAtSpec);


      processProperties(beanTypeElement, beanTypeElement, true, propertiesClassBuilder,
          pathClassBuilder, stringConstantsClassBuilder);


      propertiesClassBuilder.addType(pathClassBuilder.build());


      TypeSpec propertiesClass = propertiesClassBuilder.build();
      JavaFile propertiesJavaFile =
          JavaFile.builder(packageElement.getQualifiedName().toString(), propertiesClass).build();
      propertiesJavaFile.writeTo(processingEnv.getFiler());

      TypeSpec stringConstantsClass = stringConstantsClassBuilder.build();
      JavaFile stringConstantsJavaFile = JavaFile
          .builder(packageElement.getQualifiedName().toString(), stringConstantsClass).build();
      stringConstantsJavaFile.writeTo(processingEnv.getFiler());


      processedBeanTypes.add(beanTypeElement.getQualifiedName().toString());
    }
    catch (IOException ex)
    {
      String msg = String.format("Failed to write property constants for %s.",
          beanTypeElement.getQualifiedName());
      LOG.log(Level.SEVERE, msg, ex);
      processingEnv.getMessager().printMessage(Kind.ERROR, msg, beanTypeElement);
    }
  }

  protected void processProperties(TypeElement owningBeanTypeElement, TypeElement beanTypeElement,
      boolean includeInherited, TypeSpec.Builder propertiesClassBuilder,
      TypeSpec.Builder pathClassBuilder, TypeSpec.Builder stringConstantsClassBuilder)
  {
    ClassName propertyPathConstantsClassName =
        getPropertyPathConstantsClassName(owningBeanTypeElement);

    List<ExecutableElement> methods;
    if (includeInherited)
      methods = methodsIn(processingEnv.getElementUtils().getAllMembers(beanTypeElement));
    else
      methods = methodsIn(beanTypeElement.getEnclosedElements());
    methods.removeIf(m -> !m.getModifiers().contains(Modifier.PUBLIC)
        || m.getModifiers().contains(Modifier.STATIC));

    Map<String, ExecutableElement> setters = new HashMap<>();
    for (ExecutableElement method : methods)
    {
      String methodName = method.getSimpleName().toString();
      if (!methodName.startsWith(SET_PREFIX))
        continue;

      String propertyName = methodName.substring(SET_PREFIX.length());
      propertyName = Introspector.decapitalize(propertyName);

      setters.put(propertyName, method);
    }

    for (ExecutableElement method : methods)
    {
      String methodName = method.getSimpleName().toString();
      if (!methodName.startsWith(GET_PREFIX) && !methodName.startsWith(IS_PREFIX))
        continue;

      String propertyName;
      if (methodName.startsWith(GET_PREFIX))
        propertyName = methodName.substring(GET_PREFIX.length());
      else
        propertyName = methodName.substring(IS_PREFIX.length());

      propertyName = Introspector.decapitalize(propertyName);

      if (IGNORED_PROPERTIES.contains(propertyName))
        continue;

      String propertyNameIdentifier = propertyName;
      if (!SourceVersion.isName(propertyName))
        propertyNameIdentifier += "_";

      ExecutableType methodType = (ExecutableType) processingEnv.getTypeUtils()
          .asMemberOf((DeclaredType) beanTypeElement.asType(), method);

      TypeName propertyType = convertPropertyType(methodType.getReturnType());

      FieldSpec.Builder propertyFieldSpecBuilder = FieldSpec.builder(
          ParameterizedTypeName.get(ClassName.get(Property.class),
              ClassName.get(owningBeanTypeElement), propertyType),
          propertyNameIdentifier, Modifier.PUBLIC, Modifier.STATIC, Modifier.FINAL);
      ExecutableElement setterMethod = setters.get(propertyName);
      if (setterMethod != null)
      {
        propertyFieldSpecBuilder.initializer("new $T<>($S, $T::$L, $T::$L)", PropertyImpl.class,
            propertyNameIdentifier, ClassName.get(owningBeanTypeElement), method.getSimpleName(),
            ClassName.get(owningBeanTypeElement), setterMethod.getSimpleName());
      }
      else
      {
        propertyFieldSpecBuilder.initializer("new $T<>($S, $T::$L, null)", PropertyImpl.class,
            propertyNameIdentifier, ClassName.get(owningBeanTypeElement), method.getSimpleName());
      }
      FieldSpec propertyFieldSpec = propertyFieldSpecBuilder.build();

      MethodSpec propertyMethodSpec = null;
      MethodSpec propertyPathMethodSpec = null;

      TypeMirror returnType = method.getReturnType();
      if (returnType.getKind() == TypeKind.DECLARED)
      {
        DeclaredType declaredReturnType = (DeclaredType) returnType;
        Properties propertiesAt = declaredReturnType.asElement().getAnnotation(Properties.class);
        if (propertiesAt != null || processedBeanTypes
            .contains(((TypeElement) declaredReturnType.asElement()).getQualifiedName().toString()))
        {
          // property to other bean with generated properties constants

          String propPackageName = processingEnv.getElementUtils().getPackageOf(beanTypeElement)
              .getQualifiedName().toString();
          String referencedBeanPropertiesClassName = PROPERTIES_PREFIX
              + declaredReturnType.asElement().getSimpleName() + PROPERTIES_SUFFIX;
          ClassName referencedBeanInnerPathTypeName =
              ClassName.get(propPackageName, referencedBeanPropertiesClassName, INNER_PATH);

          propertyMethodSpec = MethodSpec.methodBuilder(propertyNameIdentifier) //
              .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
              .returns(ParameterizedTypeName.get(referencedBeanInnerPathTypeName,
                  ClassName.get(owningBeanTypeElement)))
              .addStatement("return new $T<>(null, $L)", referencedBeanInnerPathTypeName,
                  propertyNameIdentifier)
              .build();

          propertyPathMethodSpec = MethodSpec.methodBuilder(propertyNameIdentifier) //
              .addModifiers(Modifier.PUBLIC)
              .returns(ParameterizedTypeName.get(referencedBeanInnerPathTypeName, TYPEVAR_S))
              .addStatement("return new $T<>(null, $L)", referencedBeanInnerPathTypeName,
                  propertyNameIdentifier)
              .build();
        }
      }
      if (propertyMethodSpec == null || propertyPathMethodSpec == null)
      {
        propertyMethodSpec = MethodSpec.methodBuilder(propertyNameIdentifier) //
            .addModifiers(Modifier.PUBLIC, Modifier.STATIC)
            .returns(ParameterizedTypeName.get(ClassName.get(PropertyPath.class),
                ClassName.get(owningBeanTypeElement), propertyType))
            .addStatement("return new $T<>(null, $L)", PropertyPathImpl.class,
                propertyNameIdentifier)
            .build();

        propertyPathMethodSpec = MethodSpec.methodBuilder(propertyNameIdentifier) //
            .addModifiers(Modifier.PUBLIC)
            .returns(ParameterizedTypeName.get(ClassName.get(PropertyPath.class), TYPEVAR_S,
                propertyType))
            .addStatement("return new $T<>(this, $T.$L)", PropertyPathImpl.class,
                propertyPathConstantsClassName, propertyNameIdentifier)
            .build();
      }


      FieldSpec propertyNameFieldSpec = FieldSpec.builder(String.class, propertyNameIdentifier,
          Modifier.PUBLIC, Modifier.STATIC, Modifier.FINAL).initializer("$S", propertyName).build();
      stringConstantsClassBuilder.addField(propertyNameFieldSpec);

      propertiesClassBuilder.addField(propertyFieldSpec);
      propertiesClassBuilder.addMethod(propertyMethodSpec);

      pathClassBuilder.addMethod(propertyPathMethodSpec);
    }
  }

  private ClassName getPropertyPathConstantsClassName(TypeElement beanTypeElement)
  {
    PackageElement packageElement = processingEnv.getElementUtils().getPackageOf(beanTypeElement);

    String className = ProcessorUtils.getTypeNesting(beanTypeElement).stream()
        .map(te -> te.getSimpleName().toString())
        .collect(joining("_", PROPERTIES_PREFIX, PROPERTIES_SUFFIX));

    return ClassName.get(packageElement.getQualifiedName().toString(), className);
  }

  private ClassName getStringConstantsClassName(TypeElement beanTypeElement)
  {
    PackageElement packageElement = processingEnv.getElementUtils().getPackageOf(beanTypeElement);

    String className = ProcessorUtils.getTypeNesting(beanTypeElement).stream()
        .map(te -> te.getSimpleName().toString())
        .collect(joining("_", PROPERTIES_PREFIX, STRING_CONSTANTS_SUFFIX));

    return ClassName.get(packageElement.getQualifiedName().toString(), className);
  }

  private TypeName convertPropertyType(TypeMirror propertyTypeMirror)
  {
    TypeName propertyType;

    switch (propertyTypeMirror.getKind())
    {
      case BOOLEAN:
        propertyType = TypeName.BOOLEAN.box();
        break;

      case BYTE:
        propertyType = TypeName.BYTE.box();
        break;

      case SHORT:
        propertyType = TypeName.SHORT.box();
        break;

      case INT:
        propertyType = TypeName.INT.box();
        break;

      case LONG:
        propertyType = TypeName.LONG.box();
        break;

      case FLOAT:
        propertyType = TypeName.FLOAT.box();
        break;

      case DOUBLE:
        propertyType = TypeName.DOUBLE.box();
        break;

      case CHAR:
        propertyType = TypeName.CHAR.box();
        break;

      case TYPEVAR:
        propertyType = TypeName.get(((TypeVariable) propertyTypeMirror).getUpperBound());
        break;

      default:
        propertyType = TypeName.get(propertyTypeMirror);
        break;
    }

    return propertyType;
  }

}
