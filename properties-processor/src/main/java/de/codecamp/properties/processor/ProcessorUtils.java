package de.codecamp.properties.processor;

import static java.util.stream.Collectors.toList;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;

import javax.annotation.processing.ProcessingEnvironment;
import javax.lang.model.element.AnnotationMirror;
import javax.lang.model.element.AnnotationValue;
import javax.lang.model.element.Element;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Elements;


class ProcessorUtils
{

  private final ProcessingEnvironment processingEnv;

  private Optional<TypeElement> generatedAnnotation;


  public ProcessorUtils(ProcessingEnvironment processingEnv)
  {
    this.processingEnv = processingEnv;
  }


  public Optional<TypeElement> getGeneratedAnnotation(Elements elements)
  {
    if (generatedAnnotation == null)
    {
      TypeElement typeElement = elements.getTypeElement("javax.annotation.processing.Generated");
      if (typeElement == null)
        typeElement = elements.getTypeElement("javax.annotation.Generated");
      generatedAnnotation = Optional.ofNullable(typeElement);
    }
    return generatedAnnotation;
  }

  public static List<TypeElement> getTypeNesting(TypeElement typeElement)
  {
    List<TypeElement> result = new ArrayList<>();

    while (typeElement != null)
    {
      result.add(0, typeElement);

      typeElement = getEnclosingTypeElement(typeElement);
    }
    return result;
  }

  private static TypeElement getEnclosingTypeElement(Element element)
  {
    do
    {
      element = element.getEnclosingElement();
    }
    while (element != null && !element.getKind().isClass() && !element.getKind().isInterface());

    return (TypeElement) element;
  }

  public AnnotationMirror getAnnotationMirror(Element element,
      Class<? extends Annotation> annotationType)
  {
    for (AnnotationMirror am : element.getAnnotationMirrors())
    {
      if (am.getAnnotationType().toString().equals(annotationType.getName()))
      {
        return am;
      }
    }
    return null;
  }

  private AnnotationValue getAnnotationValue(AnnotationMirror annotationMirror, String key)
  {
    for (Entry<? extends ExecutableElement, ? extends AnnotationValue> entry : annotationMirror
        .getElementValues().entrySet())
    {
      if (entry.getKey().getSimpleName().toString().equals(key))
      {
        return entry.getValue();
      }
    }
    return null;
  }

  @SuppressWarnings("unchecked")
  private List<AnnotationValue> getAnnotationValues(AnnotationMirror annotationMirror, String key)
  {
    for (Entry<? extends ExecutableElement, ? extends AnnotationValue> entry : annotationMirror
        .getElementValues().entrySet())
    {
      if (entry.getKey().getSimpleName().toString().equals(key))
      {
        AnnotationValue annotationValue = getAnnotationValue(annotationMirror, key);
        return (List<AnnotationValue>) annotationValue.getValue();
      }
    }
    // empty arrays in annotations are not contained among element values
    return Collections.emptyList();
  }

  public List<TypeElement> getAnnotationValueAsTypes(AnnotationMirror annotationMirror, String key)
  {
    List<AnnotationValue> values = getAnnotationValues(annotationMirror, key);
    if (values == null)
      return null;

    return values.stream()
        .map(av -> (TypeElement) processingEnv.getTypeUtils().asElement((TypeMirror) av.getValue()))
        .collect(toList());
  }

}
