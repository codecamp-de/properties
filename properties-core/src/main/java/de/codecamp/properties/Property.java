package de.codecamp.properties;

import java.io.Serializable;
import java.util.function.BiConsumer;
import java.util.function.Function;


/**
 * Represents a bean property. Besides the property name, it also contains the bean and property
 * type. Additionally it can be used as starting point to create a {@link PropertyPath}.
 *
 * @param <B>
 *          the bean type
 * @param <P>
 *          the property type
 */
public interface Property<B, P>
  extends PropertyPath<B, P>
{

  /**
   * Returns the name of the property.
   *
   * @return the name of the property
   */
  String getName();


  @FunctionalInterface
  public interface ValueGetter<B, P>
    extends Function<B, P>, Serializable
  {
  }

  @FunctionalInterface
  public interface ValueSetter<B, P>
    extends BiConsumer<B, P>, Serializable
  {
  }

}
