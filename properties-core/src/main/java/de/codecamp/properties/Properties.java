package de.codecamp.properties;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * Classes annotated with {@link Properties} will have constants generated for each of their bean
 * properties. This includes a simple string constant containing the name of the property (useful
 * for referencing them in annotations where dynamically generated values are not allowed) and the
 * more complex {@link Property} constant that provides additional information about the property
 * and that can be used as a starting point to create {@link PropertyPath property paths}.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Properties
{
}
