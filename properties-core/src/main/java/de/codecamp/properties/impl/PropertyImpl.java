package de.codecamp.properties.impl;

import de.codecamp.properties.Property;


public class PropertyImpl<B, P>
  implements Property<B, P>
{

  private final String name;

  private final ValueGetter<B, P> getter;

  private final ValueSetter<B, P> setter;


  public PropertyImpl(String name, ValueGetter<B, P> getter, ValueSetter<B, P> setter)
  {
    this.name = name;
    this.getter = getter;
    this.setter = setter;
  }


  @Override
  public String getName()
  {
    return name;
  }


  @Override
  public P get(B bean)
  {
    return getter.apply(bean);
  }

  @Override
  public void set(B bean, P value)
  {
    if (setter == null)
      throw new IllegalStateException("property is read-only");
    setter.accept(bean, value);
  }


  @Override
  public Property<?, ?>[] toProperties()
  {
    return new Property<?, ?>[] {this};
  }

  @Override
  public String toPathString()
  {
    return getName();
  }


  @Override
  public String toString()
  {
    return getName();
  }

}
