package de.codecamp.properties.impl;

import static java.util.stream.Collectors.joining;

import java.util.stream.Stream;

import de.codecamp.properties.Property;
import de.codecamp.properties.PropertyPath;


public class PropertyPathImpl<S, T>
  implements PropertyPath<S, T>
{

  private final Property<?, ?>[] path;

  private String pathString;


  public <M> PropertyPathImpl(PropertyPath<S, M> path, Property<M, T> property)
  {
    if (path != null)
    {
      Property<?, ?>[] propertyPath = path.toProperties();
      Property<?, ?>[] combinedPropertyPath = new Property[propertyPath.length + 1];
      System.arraycopy(propertyPath, 0, combinedPropertyPath, 0, propertyPath.length);
      combinedPropertyPath[propertyPath.length] = property;
      this.path = combinedPropertyPath;
    }
    else
    {
      this.path = new Property<?, ?>[] {property};
    }
  }


  @Override
  public Property<?, ?>[] toProperties()
  {
    return path.clone();
  }

  @Override
  public String toPathString()
  {
    if (pathString == null)
    {
      pathString = Stream.of(path).map(p -> p.getName()).collect(joining("."));
    }
    return pathString;
  }


  @Override
  public String toString()
  {
    return Stream.of(path).map(p -> p.getName()).collect(joining("."));
  }

}
