package de.codecamp.properties;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * Classes referenced by this annotation are treated as if they were annotated with
 * {@link Properties} directly. This is useful to create property constants for classes in other
 * projects. Can be placed on any class.
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(PropertiesFor.Container.class)
@Documented
public @interface PropertiesFor
{

  Class<?>[] value();


  /**
   * Container for repeated {@link PropertiesFor @PropertiesFor} annotations.
   */
  @Target({ElementType.TYPE})
  @Retention(RetentionPolicy.RUNTIME)
  @Documented
  public @interface Container
  {

    /**
     * Returns the {@link PropertiesFor @PropertiesFor} annotations.
     *
     * @return the {@link PropertiesFor @PropertiesFor} annotations
     */
    PropertiesFor[] value();
  }

}
