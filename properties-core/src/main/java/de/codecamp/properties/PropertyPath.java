package de.codecamp.properties;

import java.io.Serializable;


/**
 * A {@link PropertyPath} represents a path of nested/linked bean properties from a source bean. A
 * {@link Property} is the most simple property path.
 *
 * @param <S>
 *          source type
 * @param <T>
 *          target type
 */
public interface PropertyPath<S, T>
  extends Serializable
{

  /**
   * Returns the individual properties in this path.
   *
   * @return the individual properties in this path
   */
  Property<?, ?>[] toProperties();

  /**
   * Returns the path in a dot notation.
   *
   * @return the path in dot notation
   */
  String toPathString();


  /**
   * Returns the value at the end of the property path starting from the given source bean. For
   * convenience, if any property of a nested/linked bean is null, null is returned.
   *
   * @param sourceBean
   *          the source bean
   * @return the property value
   */
  @SuppressWarnings({"unchecked", "rawtypes"})
  default T get(S sourceBean)
  {
    Object bean = sourceBean;
    for (Property property : toProperties())
    {
      bean = property.get(bean);
      if (bean == null)
        return null;
    }
    return (T) bean;
  }

  @SuppressWarnings({"unchecked", "rawtypes"})
  default void set(S sourceBean, T value)
  {
    Object bean = sourceBean;
    Property[] propertyChain = toProperties();
    for (int i = 0; i < propertyChain.length; i++)
    {
      Property property = propertyChain[i];
      if (i == propertyChain.length - 1)
      {
        property.set(bean, value);
        return;
      }
      else
      {
        bean = property.get(bean);
        if (bean == null)
        {
          StringBuilder sb = new StringBuilder();
          for (int p = 0; p <= i; p++)
          {
            if (p > 0)
              sb.append(".");
            sb.append(propertyChain[p].getName());
          }
          throw new IllegalArgumentException(String
              .format("Failed to set property. A property on the path there (%s) is null.", sb));
        }
      }
    }
  }

}
